Rails.application.routes.draw do
  root 'home#index'

  get 'questions', to: 'home#index', as: 'questions_view'
  get 'quiz', to: 'home#index'

  scope '/api' do
    resources :questions

    get 'quiz', to: 'quiz#get'
    post 'quiz', to: 'quiz#check'
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
