class QuizController < ApplicationController
  def get
    questions = Question.all
    i = (questions.length * rand).to_i
    q = questions[i]
    render json: { id: q[:id], text: q[:text] }
  end

  def check
    answer = params[:answer]
    q = Question.find(params[:id])
    render json: { result: q.check_answer(answer) }
  end
end
