class QuestionsController < ApplicationController
  def index
    questions = Question.all
    render json: (questions.map { |q| q.sanitized })
  end

  def show
    question = Question.find(params[:id])
    render json: question.sanitized
  end

  def create
    question = Question.new(question_params)
    question.save
    render json: question
  end

  def update
    question = Question.find(params[:id])
    question.update(question_params)
    render json: question
  end

  def destroy
    question = Question.find(params[:id])
    question.destroy
    render json: question
  end

  private
    def question_params
      params.require(:question).permit(:text, :answer)
    end
end
