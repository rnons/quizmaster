class Question < ApplicationRecord
  @@white_list_sanitizer = Rails::Html::WhiteListSanitizer.new

  def check_answer answer
    normalize_answer(self.answer) == normalize_answer(answer)
  end

  def sanitized
    {
      id: self.id,
      text: sanitize(self.text),
      answer: self.answer
    }
  end

  private
    def sanitize string
      @@white_list_sanitizer.sanitize(
        string, tags: %w(strong italic b i), attributes: %w(style))
    end

    def is_int? string
      true if Integer(string) rescue false
    end

    def normalize_answer string
      string.split.map do |w|
        is_int?(w) ? w.to_i.humanize : w
      end
    end
end
