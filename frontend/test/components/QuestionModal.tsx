import * as React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';

import QuestionModal from '../../src/components/QuestionModal'

describe('QuestionModal', () => {
  it ('should render correctly', () => {
    const cmp = shallow(
      <QuestionModal
        isOpen={true}
        title="test"
        question={{ text: 'text', answer: 'answer' }}
        onClose={() => {}}
        onSubmit={() => { return Promise.resolve() }}
        />)
    expect(cmp.find('textarea')).to.have.length(2);
    expect(cmp.find('button')).to.have.length(1);
  })

  it ('should call onSubmit', () => {
    const cmp = mount(
      <QuestionModal
        isOpen={true}
        title="test"
        question={{ text: 'text', answer: 'answer' }}
        onClose={() => {}}
        onSubmit={() => { console.log('submit'); return Promise.resolve() }}
        />)
    console.log(cmp.debug())
  })
})
