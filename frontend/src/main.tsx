import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';
import '@blueprintjs/core/dist/blueprint.css';

import App from './components/App';
import Questions from './components/Questions';
import Quiz from './components/Quiz';

ReactDOM.render(
  <Router history={browserHistory}>
    <Route path="/"
           component={App}>
      <Route path="questions"
             component={Questions} />
      <Route path="quiz"
             component={Quiz} />
    </Route>
  </Router>,
  document.getElementById('root')
)
