const getMetaContent = (name) => {
  const $meta = document.querySelector(`meta[name="${name}"]`);
  return $meta ? $meta.getAttribute('content') : undefined;
}

const csrfParam = getMetaContent('csrf-param');
const csrfToken = getMetaContent('csrf-token');

const checkStatus = (response) => {
  if (response.status >= 200 && response.status < 300) {
    return response;
  } else {
    const error = new Error(response.statusText);
    throw error;
  }
};

const parseJson = (response) => response.json();

const Http = {
  get(url) {
    return fetch(url, {
      credentials: 'same-origin'
    }).then(checkStatus)
      .then(parseJson);
  },

  _post(url, data, method='post') {
    return fetch(url, {
      method,
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(
        Object.assign({}, data, {
          [csrfParam]: csrfToken
        })
      )
    }).then(checkStatus)
      .then(parseJson);
  },

  post(url, data) {
    return this._post(url, data);
  },

  patch(url, data) {
    return this._post(url, data, 'PATCH');
  },

  delete(url) {
    return this._post(url, {}, 'DELETE');
  }
};

export default Http;
