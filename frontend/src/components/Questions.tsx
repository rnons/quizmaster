import * as React from 'react';
import { Dialog } from '@blueprintjs/core';

import Http from '../utils/http';
import QuestionModal from './QuestionModal';


interface State {
  questions?: any[];
  isModalOpen?: boolean;
  modalTitle?: string;
  modalQuestion?: any;
}

class Questions extends React.Component<undefined, State> {
  state: State = {
    questions: [],
    isModalOpen: false,
    modalTitle: '',
    modalQuestion: undefined,
  };

  componentDidMount() {
    document.title = 'Mangage questions - Quiz Master'
    Http.get('/api/questions').then(questions => {
      this.setState({
        questions
      })
    })
  }

  handleCloseModal = () => {
    this.setState({
      isModalOpen: false
    });
  }

  handleSubmit = (question) => {
    if (question.id) {
      return this.editQuestion(question);
    } else {
      return this.createQuestion(question);
    }
  }

  showCreateModal = () => {
    this.setState({
      isModalOpen: true,
      modalTitle: 'New question',
      modalQuestion: undefined
    });
  }

  showEditModal = (question) => {
    this.setState({
      isModalOpen: true,
      modalTitle: 'Edit question',
      modalQuestion: question
    });
  }

  createQuestion = (question) => {
    return Http.post('/api/questions', question).then(question => {
      this.setState({
        questions: [...this.state.questions, question]
      })
    });
  }

  editQuestion = (question) => {
    const { questions } = this.state;
    return Http.patch(`/api/questions/${question.id}`, {
      text: question.text,
      answer: question.answer
    }).then(question => {
      const index = questions.findIndex(q => q.id === question.id);
      const newQuestions = [
        ...questions.slice(0, index), question, ...questions.slice(index + 1)
      ];
      this.setState({
        questions: newQuestions
      });
    });
  }

  deleteQuestion = (question) => {
    if (!confirm('Deletion cannot be undone, continue?')) {
      return
    }
    const { questions } = this.state;
    return Http.delete(`/api/questions/${question.id}`).then(question => {
      const index = questions.findIndex(q => q.id === question.id);
      questions.splice(index, 1);
      this.setState({
        questions
      });
    });
  }

  render() {
    const { questions, isModalOpen, modalTitle, modalQuestion } = this.state;
    const list = questions.map((question, index) => {
      return (
        <li
          style={{
            listStyle: 'none',
            borderBottom: '1px solid lightgray',
            padding: '0.5rem',
            display: 'flex',
            justifyContent: 'space-between',
          }}
          key={index}>
          <div>
            <div>
              <strong>Q: </strong>
              <span dangerouslySetInnerHTML={{ __html: question.text }}></span>
            </div>
            <div>
              <strong>A: </strong>{question.answer}
            </div>
          </div>
          <div>
            <button
              className="pt-button"
              type="button"
              onClick={this.showEditModal.bind(null, question)}
              >Edit</button>
            <button
              className="pt-button pt-intent-danger"
              type="button"
              onClick={this.deleteQuestion.bind(null, question)}
              >Delete</button>
          </div>
        </li>
      );
    })
    return (
      <div>
        <div style={{ padding: '1rem 0' }}>
          Questions ({ questions.length })
          <button
            className="pt-button pt-intent-primary"
            style={{ marginLeft: '1rem' }}
            onClick={this.showCreateModal}>new question</button>
        </div>
        <QuestionModal
          isOpen={isModalOpen}
          title={modalTitle}
          question={modalQuestion}
          onClose={this.handleCloseModal}
          onSubmit={this.handleSubmit}
          />
        <ul style={{ margin: 0, padding: 0 }}>
          { list }
        </ul>
      </div>
    );
  }
};

export default Questions;
