import * as React from 'react';
import { Dialog } from '@blueprintjs/core';

const ResultModal = (props) => {
  const { isOpen, answer, result, title, onClose, onNext } = props;
  return (
    <Dialog
      isOpen={isOpen}
      onClose={onClose}
      title={title}
      >
      <div style={{ padding: '1rem' }}>
        <div style={{ marginBottom: '2rem' }}>
          You answered { answer }, which is { result ? 'correct' : 'incorrect' }
        </div>
        { !result &&
          <button
            className="pt-button"
            type="button"
            onClick={onClose}
            >Retry</button>
        }
        <button
          className="pt-button pt-intent-primary"
          type="button"
          onClick={onNext}
          >Next quiz</button>
      </div>
    </Dialog>
  );
};

export default ResultModal;
