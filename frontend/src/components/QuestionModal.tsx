import * as React from 'react';
import { Dialog } from '@blueprintjs/core';


interface Question {
  text: string;
  answer: string;
}

interface Props {
  isOpen: boolean;
  title: string;
  question: Question;
  onClose: () => void;
  onSubmit: (q: Question) => Promise<any>;
}

const emptyQuestion = {
  text: '',
  answer: ''
}

class QuestionModal extends React.Component<Props, any> {
  props: Props;

  state = emptyQuestion;

  componentWillReceiveProps(nextProps) {
    const { question = emptyQuestion } = nextProps;
    this.setState({
      text: question.text,
      answer: question.answer
    });
  }

  handleChange = (name, event) => {
    const value = event.currentTarget.value;
    this.setState({
      [name]: value
    });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const { text, answer } = this.state;
    this.props.onSubmit(Object.assign({}, this.props.question, {
      text,
      answer
    })).then(this.props.onClose);
  }

  render() {
    const { isOpen, title, onClose } = this.props;
    const { text, answer } = this.state;
    return (
      <Dialog
        iconName="inbox"
        isOpen={isOpen}
        onClose={onClose}
        title={title}
        >
        <form
          style={{ padding: '1rem' }}
          onSubmit={this.handleSubmit}
          >
          <label className="pt-label">
            Question
            <textarea
              className="pt-input pt-fill"
              value={text}
              onChange={this.handleChange.bind(null, 'text')}
              ></textarea>
          </label>
          <label className="pt-label">
            Answer
            <textarea
              className="pt-input pt-fill"
              value={answer}
              onChange={this.handleChange.bind(null, 'answer')}
              ></textarea>
          </label>
          <button
            className="pt-button pt-intent-primary"
            type="submit"
            >Save</button>
        </form>
      </Dialog>
    );
  }
};

export default QuestionModal;
