import * as React from 'react';
import Http from '../utils/http';

import ResultModal from './QuizResultModal';


interface Question {
  id: string;
  text: string;
}

interface State {
  question?: Question;
  answer?: string;
  isModalOpen?: boolean;
  result?: boolean;
}

class Quiz extends React.Component<undefined, State> {
  state = {
    question: {
      id: '',
      text: ''
    },
    answer: '',
    isModalOpen: false,
    result: false,
  }

  componentDidMount() {
    document.title = 'Take a quiz - Quiz Master'
    this.handleNext()
  }

  handleChange = (name, event) => {
    const value = event.currentTarget.value;
    this.setState({
      [name]: value
    });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const { question, answer } = this.state;
    if (!answer.trim()) return;
    Http.post('/api/quiz', {
      id: question.id,
      answer
    }).then(data => {
      console.log(data)
      this.setState({
        isModalOpen: true,
        result: data.result
      })
    })
  }

  handleCloseModal = () => {
    this.setState({
      isModalOpen: false
    })
  }

  handleNext = () => {
    Http.get('/api/quiz').then(question => {
      this.setState({
        question,
        answer: '',
        isModalOpen: false
      })
    })
  }

  render() {
    const { question, answer, isModalOpen, result } = this.state;
    return (
      <div style={{
        width: '20rem',
        margin: '1.25rem auto',
      }} >
        <div style={{
          textAlign: 'center',
          fontSize: '1.25rem',
          fontWeight: 'bold',
        }}>
          Let's take a quiz
        </div>
        <form onSubmit={this.handleSubmit}>
          <label className="pt-label">
            Question
            <div dangerouslySetInnerHTML={{ __html: question.text }}></div>
          </label>
          <label className="pt-label">
            Please write your answer
            <textarea
              className="pt-input pt-fill"
              value={answer}
              onChange={this.handleChange.bind(null, 'answer')}
              ></textarea>
          </label>
          <button
            className="pt-button pt-intent-primary"
            type="submit"
            >Submit</button>
        </form>
        <ResultModal
          isOpen={isModalOpen}
          answer={answer}
          result={result}
          title="Result"
          onClose={this.handleCloseModal}
          onNext={this.handleNext}
          />
      </div>
    );
  };
}

export default Quiz;
