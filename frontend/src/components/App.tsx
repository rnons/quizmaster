import * as React from 'react';
import { Link } from 'react-router';

const App = (props) => {
  const { children } = props;
  return (
    <div>
      <nav className="pt-navbar .modifier">
        <div className="pt-navbar-group pt-align-left">
          <div className="pt-navbar-heading">Quiz Master</div>
        </div>
        <div className="pt-navbar-group pt-align-right">
          <Link
            className="pt-button pt-minimal pt-icon-database"
            to="questions">Manage Questions</Link>
          <Link
            className="pt-button pt-minimal pt-icon-ninja"
            to="quiz">Take a quiz</Link>
        </div>
      </nav>
      { children }
    </div>
  );
};

export default App;
