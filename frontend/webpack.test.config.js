module.exports = {
  devtool: 'inline-source-map',
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
  },
  module: {
    loaders: [
      { test: /\.tsx?$/, loader: 'awesome-typescript-loader' },
    ],
  },
  externals: {
    'jsdom': 'window',
    'cheerio': 'window',
    'react/addons': 'window',
    'react/lib/ReactContext': 'window',
    'react/lib/ExecutionEnvironment': true
  },
};
