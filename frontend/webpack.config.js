const webpack = require('webpack');
const { CheckerPlugin } = require('awesome-typescript-loader')


module.exports = {
  context: __dirname,
  devtool: 'source-map',
  target: 'web',
  entry: './src/main.tsx',
  output: {
    path: __dirname + '/../public/dist',
    filename: 'main.js',
    publicPath: '/dist/'
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js']
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      }, {
        test: /\.tsx?$/,
        use: [
          'awesome-typescript-loader',
        ]
      }, {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: 'url-loader?limit=10000&mimetype=application/font-woff'
      }, {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: 'file-loader'
      }
    ]
  },
  plugins: [
    new CheckerPlugin()
  ]
};
