# README

## Setup

### backend

```
bin/setup
bin/rails server
```

### frontend

```
cd frontend
npm i
npm start
```

Visit http://localhost:3000

## Test

### backend

```
bin/rails test
```

### frontend

```
cd frontend
npm test
```
