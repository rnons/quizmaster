require 'test_helper'

class QuestionTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test "check_answer should work" do
    question = questions(:one)
    assert question.check_answer('oneAnswer')
    assert_not question.check_answer('one Answer')
  end

  test "check_answer should work for numbers" do
    question = questions(:two)
    assert question.check_answer('42')
    assert question.check_answer('forty-two')
  end

  test "sanitized should whitelist formatting tags" do
    question = questions(:three)
    assert_equal '<strong>important</strong> alert("hi")', question.sanitized[:text]
  end
end
