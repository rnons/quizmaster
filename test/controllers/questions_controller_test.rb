require 'test_helper'

class QuestionsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get questions_url
    assert_response :success
  end

  test "should show question" do
    question = questions(:one)
    get question_url(question), xhr: true
    ret = JSON.parse(@response.body)
    assert_equal 'oneText', ret['text']
    assert_equal 'oneAnswer', ret['answer']
  end

  test "should create a question" do
    assert_difference('Question.count') do
      post questions_url, params: { question: { text: 'newText', answer: 'newAnswer' } }, xhr: true
      ret = JSON.parse(@response.body)
      assert_equal 'newText', ret['text']
      assert_equal 'newAnswer', ret['answer']
    end
  end

  test "should edit a question" do
    question = questions(:two)
    patch question_url(question), params: { question: { text: 'editedText', answer: 'editedAnswer' } }, xhr: true
    ret = JSON.parse(@response.body)
    assert_equal 'editedText', ret['text']
    assert_equal 'editedAnswer', ret['answer']
  end

  test "should delete a question" do
    question = questions(:one)
    assert_difference('Question.count', -1) do
      delete question_url(question)
    end
  end

end
